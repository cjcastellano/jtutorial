package com.elavon.testing;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
public class ExerciseSelenium {



	WebDriver driver;
	
	@Before
	public void setup(){
		
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--start-maximized");
		driver = new ChromeDriver(chromeOptions);
		
	}
	

	@Test
	public void FirstTestCase() throws InterruptedException {
	
		
		driver.get("http://demoqa.com/");
		driver.findElement(By.id("menu-item-374")).click();
        WebElement searchfirstname = driver.findElement(By.id("name_3_firstname"));
		searchfirstname.sendKeys("Carlos Joel");
		WebElement searchlastname = driver.findElement(By.id("name_3_lastname"));
		searchlastname.sendKeys("Castellano");
		driver.findElement(By.name("radio_4[]")).click();
		driver.findElement(By.name("checkbox_5[]")).click();
		WebElement sel = driver.findElement(By.id("dropdown_7"));
		Select languageDropdown = new Select(sel);
		languageDropdown.selectByValue("Philippines");
		Select dateMonth = new Select(driver.findElement(By.id("mm_date_8")));
		dateMonth.selectByValue("12");
		Select dateDay = new Select(driver.findElement(By.id("dd_date_8")));
		dateDay.selectByValue("28");
		Select dateYear = new Select(driver.findElement(By.id("yy_date_8")));
		dateYear.selectByValue("1994");
		JavascriptExecutor je = (JavascriptExecutor)driver;
		
	     WebElement phoneNumber = driver.findElement(By.id("phone_9"));
		phoneNumber.sendKeys("09051161898");
		
		je.executeScript("arguments[0].scrollIntoView(true);",phoneNumber);
	    WebElement userName = driver.findElement(By.id("username"));
		userName.sendKeys("cjtwentyfive");
		WebElement email = driver.findElement(By.id("email_1"));
		email.sendKeys("carlos.castellano@elavon.com");
		WebElement initialPassword = driver.findElement(By.id("password_2"));	
		initialPassword.sendKeys("Automation2017..");
		WebElement confirmPassword = driver.findElement(By.id("confirm_password_password_2"));	
		confirmPassword.sendKeys("Automation2017..");
		driver.findElement(By.name("pie_submit")).click();
	}
	@Test
	public void SecondTestCase() throws InterruptedException {
		driver.get("http://demoqa.com/");
		TimeUnit.SECONDS.sleep(15);
		driver.findElement(By.id("menu-item-374")).click();
		driver.findElement(By.cssSelector("a[href='http://demoqa.com/contact/']")).getAttribute("href");
		  WebElement searchfirstname = driver.findElement(By.id("name_3_firstname"));
			searchfirstname.sendKeys("Carlos Joel");
			WebElement searchlastname = driver.findElement(By.id("name_3_lastname"));
			searchlastname.sendKeys("Castellano");
			driver.findElement(By.name("radio_4[]")).click();
			driver.findElement(By.name("checkbox_5[]")).click();
			WebElement sel = driver.findElement(By.id("dropdown_7"));
			Select languageDropdown = new Select(sel);
			languageDropdown.selectByValue("Philippines");
			Select dateMonth = new Select(driver.findElement(By.id("mm_date_8")));
			dateMonth.selectByValue("12");
			Select dateDay = new Select(driver.findElement(By.id("dd_date_8")));
			dateDay.selectByValue("28");
			Select dateYear = new Select(driver.findElement(By.id("yy_date_8")));
			dateYear.selectByValue("1994");
			JavascriptExecutor je = (JavascriptExecutor)driver;
			
		     WebElement phoneNumber = driver.findElement(By.id("phone_9"));
			phoneNumber.sendKeys("09051161898");
			
			je.executeScript("arguments[0].scrollIntoView(true);",phoneNumber);
		    WebElement userName = driver.findElement(By.id("username"));
			userName.sendKeys("cjtwentyfive");
			WebElement email = driver.findElement(By.id("email_1"));
			email.sendKeys("carlos.castellano@elavon.com");
			WebElement initialPassword = driver.findElement(By.id("password_2"));	
			initialPassword.sendKeys("Automation2017..");
			WebElement confirmPassword = driver.findElement(By.id("confirm_password_password_2"));	
			confirmPassword.sendKeys("Automation2017..");
			driver.findElement(By.name("pie_submit")).click();
		//System.out.println(driver.getPageSource());
	}
			

			
	@After
	public void cleanup(){
	//	driver.quit();
	}

}


