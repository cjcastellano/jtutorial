package com.elavon.exercisepart2;

import com.elavon.exercise3.AwesomeCalculator;

public  class calculator implements AwesomeCalculator {
	int sum,quo,remainder;
	double difference,product,cel,fah,kg,lb;
	
	
	
	public int getSum(int augend, int addend){
	sum= augend+addend;

	return  sum;
	
	}
	
	public double getDifference(double minuend, double subtrahend)
	{
		difference = minuend - subtrahend;
		
		return difference;
		
	}

	public double getProduct(double multiplicand, double multiplier)
	{
		product = multiplicand * multiplier;
	
		return product;
	}
	public String getQuotientAndRemainder(int dividend, int divisor)
	{
		remainder=dividend%divisor;
		quo=dividend/divisor;
		
		String rem = String.valueOf(remainder);
		String quot = String.valueOf(quo);
		

		
		return "the Quotient is: "+ quot +" "+ "The Remainder is:"+ rem;
		
		
	}

	
	public double toCelsius(int fahrenheit)
	
	{
	 cel = (fahrenheit-32)*.556;
	
		return cel;
	}
	
	public double toFahrenheit(int celsius)
	{
		fah = celsius*1.8+32;
		return fah;
	}
	
	public double toKilogram(double lbs)
	{
		kg= lbs*0.453592;
	
		return kg;
	}
	
	public double toPound(double kg)
	{
	lb=kg*2.2;

	return lb;
	}
	
	public boolean isPalindrome(String str)
	{
		 int length = str.length();
		    if (length < 2) return true;
		    else return str.charAt(0) != str.charAt(length - 1) ? false :
		            isPalindrome(str.substring(1, length - 1));
		}
		
	}
	
	
	
	

