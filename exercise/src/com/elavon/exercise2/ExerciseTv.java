package com.elavon.exercise2;



public class ExerciseTv {
	protected String brand,model;
	protected int channel,volume;
	protected boolean powerOn;
	
	
	public ExerciseTv(String b, String m, int c, int v, boolean p) {
	      brand=b;
	      model=m;
				
		channel=c;
		volume=v;
		powerOn=p;
	
		
	}
	


	public ExerciseTv() {
		// TODO Auto-generated constructor stub
	}


	public boolean turnOn() {
		
		powerOn=true;
		
		return (powerOn); 
	}
	
	public boolean turnOff() {
		powerOn=false;
		return (powerOn);
	}
	public void channelUp() {
	
		channel=channel+1;
	
	}
	public void channelDown() {
		channel=channel-1;
		
	}
	
   public void volumeUp() {
	   
	volume+=1;
		
	}
	public void volumeDown() {
		volume-=1;
	
	}
	public String toString() {
		
		
		
		
		
		return brand + " " + model + "[  " + "on:" + powerOn +",  "+
		"channel:" +channel +",    " + "volume:" + volume + " ]";
	
		//eg. samsung Alpha [ on:true, channel:0, volume:5 ]
	}
	
	public static void println(){
		
		ExerciseTv tv = new ExerciseTv("Andre Electronics ", "one",0,5,false);
		System.out.println(tv.toString());
		
		
		System.out.println(tv.turnOn());
	    for ( int i=0;i<5;i++)
		{
			tv.channelUp();
		}
		tv.channelDown();
		
		
		
		for (int v=0; v<3;v++)
		{
			tv.volumeDown();
		}
		tv.volumeUp();
		tv.turnOff();
		System.out.println(tv.toString());

		
	}
	public static void main(String[] args) {
		
		
	
		ExerciseTv.println();
		
		

	}



	

	}
	
	



	 
	
	
