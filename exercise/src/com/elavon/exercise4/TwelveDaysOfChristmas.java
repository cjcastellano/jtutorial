package com.elavon.exercise4;

public class TwelveDaysOfChristmas {
	public static String numbersToLetter(int i) {

		switch (i) {
		case 1:
			return "First";
		case 2:
			return "Second";
		case 3:
			return "Third";
		case 4:
			return "Fourth";
		case 5:
			return "Fifth";
		case 6:
			return "Sixth";
		case 7:
			return "Seventh";
		case 8:
			return "Eigth";
		case 9:
			return "Nineth";
		case 10:
			return "Tenth";
		case 11:
			return "Eleventh";
		case 12:
			return "Twelve";

		}
		return null;

	}

	public static void printLyrics() {

		String First = "A Partridge in a Pear Tree.";
		String Second = "Two Turtle Doves,\nAnd " + First;
		String Third = "Three French Hens,\n" + Second;
		String Fourth = "Four Calling Birds,\n" + Third;
		String Fifth = "Five Golden Ring,\n" + Fourth;
		String Sixth = "Geese a Laying,\n" + Fifth;
		String Seventh = "Seven Swans are Swimming,\n" + Sixth;
		String Eighth = "Eight Maids are Milking,\n" + Seventh;
		String Nineth = "Nine ladies dancing,\n" + Eighth;
		String Tenth = "Ten Lords a Leaping,\n" + Nineth;
		String Eleventh = "Eleven Pipers Piping,\n" + Tenth;
		String Twelve = "Twelve Drummers Drumming,\n" + Eleventh;

		for (int i = 1; i <= 12; i++) {
			System.out.println("On the " + numbersToLetter(i) + " day of Christmas my true love gave to me:");

			if (i == 1) {
				System.out.println(First + "\n");
			}

			else if (i == 2) {

				System.out.println(Second + "\n");
			}

			else if (i == 3) {

				System.out.println(Third + "\n");
			} else if (i == 4) {

				System.out.println(Fourth + "\n");
			} else if (i == 5) {

				System.out.println(Fifth + "\n");
			} else if (i == 6) {

				System.out.println(Sixth + "\n");
			} else if (i == 7) {

				System.out.println(Seventh + "\n");
			} else if (i == 8) {

				System.out.println(Eighth + "\n");
			} else if (i == 9) {

				System.out.println(Nineth + "\n");
			} else if (i == 10) {

				System.out.println(Tenth + "\n");
			} else if (i == 11) {

				System.out.println(Eleventh + "\n");
			} else if (i == 12)

				System.out.println(Twelve + "\n");
		}

	}

	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();

	}

}