package com.elavon.exercise5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ArrayCollectionsExercise {

	public static void main(String[] args) {

		int j = 1, i = 1;
		String myStrings[];
		myStrings = new String[] { "China", "Malaysia", "Thailand", "South Korea", "Singapore", "MALDIVES", "Iraq",
				"France", "Germany", "North Korea" };
		System.out.println(myStrings.length);

		ArrayList<String> countryList = new ArrayList<String>(Arrays.asList(myStrings));
		System.out.println(countryList.size());
		countryList.add("Scotland");
		System.out.println(countryList.isEmpty());
		System.out.println(countryList.size());

		if (countryList.contains("Magnolia")) {

			System.out.println("Country found");

		} else {

			System.out.println("Country not found");

		}

		if (countryList.contains("Scotland")) {

			System.out.println("Country found");

		} else {

			System.out.println("Country not found");

		}

		countryList.remove(countryList.size() - 1);
		System.out.println(countryList.size());

		if (countryList.contains("Indonesia") && countryList.contains("United Kingdom")
				&& countryList.contains("France") && countryList.contains("Italy") && countryList.contains("USA")) {
			System.out.println("all of these top destinations made it to your list");
		} else {
			System.out.println("not TRUE");
		}

		System.out.println(countryList);

		Map<String, Integer> countryMap = new HashMap<String, Integer>();

		for (String string : countryList) {

			if (countryMap.containsKey(string)) {
				countryMap.put(i++ + string, countryMap.get(string));
			} else {
				countryMap.put(string, i++);
			}
		}

		System.out.println(countryMap);
		countryList.sort(String::compareToIgnoreCase);
		System.out.println(countryList);

		for (Map.Entry<String, Integer> entry : countryMap.entrySet()) {
			if (entry.getValue() <11){
			System.out.println("Rank #:" + j++ + " " + entry.getKey());
		}
			else {
				
			}

	}
		
	}
}

